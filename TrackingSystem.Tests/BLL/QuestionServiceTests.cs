﻿using AutoMapper;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Services;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;
using Xunit;

namespace TrackingSystem.Tests.BLL
{
    public class QuestionServiceTests
    {
        private readonly Mock<IUnitOfWork> mockIoW = new Mock<IUnitOfWork>();
        private readonly IMapper mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<QuestionDTO, CustomerQuestion>().ReverseMap()));

        [Fact]
        public async Task QuestionService_GetAllQuestionsAsync_CollectionOfQuestionDTOs()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.GetAllAsync()).Returns(GetAllQuestionAsync());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.GetAllQuestionsDTOAsync();

            // Assert
            Assert.Equal((await GetAllQuestionAsync()).Count(), actualResult.Count());
            Assert.Equal((await GetAllQuestionDTOsAsync()).ToList()[0].Id, actualResult.ToList()[0].Id);
        }

        [Fact]
        public async Task QuestionService_GetQuestionByIdAsync_QuestionDTO()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.GetByIdAsync(1)).Returns(GetQuestionAsync());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.GetQuestionDTOByIdAsync(1);

            // Assert
            Assert.NotNull(actualResult);
            Assert.Equal((await GetQuestionDTOAsync()).Id, actualResult.Id);
        }

        [Fact]
        public async Task QuestionService_GetQuestionByIdAsync_NULL()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.GetByIdAsync(It.IsAny<int>())).Returns(GetNullAsync());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.GetQuestionDTOByIdAsync(It.IsAny<int>());

            // Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task QuestionService_UpdateAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.Update(It.IsAny<CustomerQuestion>())).Returns(true);
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.UpdateAsync(It.IsAny<QuestionDTO>());

            // Assert
            Assert.True(actualResult.Succeeded);
            Assert.Equal("Updated successfully!", actualResult.Message);
        }

        [Fact]
        public async Task QuestionService_UpdateAsync_OperationDetailsNotSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.Update(It.IsAny<CustomerQuestion>())).Returns(false);
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.UpdateAsync(It.IsAny<QuestionDTO>());

            // Assert
            Assert.True(!actualResult.Succeeded);
            Assert.Equal("Message doesn`t exist.", actualResult.Message);
        }

        [Fact]
        public async Task QuestionService_DeleteAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.DeleteAsync(It.IsAny<int>())).Returns(GetTrueAsync());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.DeleteAsync(It.IsAny<int>());

            // Assert
            Assert.True(actualResult.Succeeded);
            Assert.Equal("Deleted successfully!", actualResult.Message);
        }

        [Fact]
        public async Task QuestionService_DeleteAsync_OperationDetailsNotSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.DeleteAsync(It.IsAny<int>())).Returns(GetFasleAsync());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.DeleteAsync(It.IsAny<int>());

            // Assert
            Assert.True(!actualResult.Succeeded);
            Assert.Equal("Message doesn`t exist.", actualResult.Message);
        }

        [Fact]
        public async Task QuestionService_CreateAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.QuestionRepository.Create(It.IsAny<CustomerQuestion>())).Returns(It.IsAny<CustomerQuestion>());
            var service = new QuestionService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.CreateAsync(It.IsAny<QuestionDTO>());

            // Assert
            Assert.True(actualResult.Succeeded);
        }

        #region Helper methods

        private async Task<IEnumerable<CustomerQuestion>> GetAllQuestionAsync()
        {
            return new List<CustomerQuestion>
            {
                new CustomerQuestion { Id = 1, Email = "Test1@test.com"},
                new CustomerQuestion { Id = 2, Email = "Test2@test.com"}
            };
        }

        private async Task<CustomerQuestion> GetQuestionAsync()
        {
            return new CustomerQuestion { Id = 1, Email = "Test1@test.com" };
        }

        private async Task<IEnumerable<QuestionDTO>> GetAllQuestionDTOsAsync()
        {
            return new List<QuestionDTO>
            {
                new QuestionDTO { Id = 1, Email = "Test1@test.com"},
                new QuestionDTO { Id = 2, Email = "Test2@test.com"}
            };
        }

        private IEnumerable<CustomerQuestion> GetAllQuestions()
        {
            return new List<CustomerQuestion>
            {
                new CustomerQuestion { Id = 1, Email = "Test1@test.com"},
                new CustomerQuestion { Id = 2, Email = "Test2@test.com"}
            };
        }

        private IEnumerable<QuestionDTO> GetAllQuestionDTOs()
        {
            return new List<QuestionDTO>
            {
                new QuestionDTO { Id = 1, Email = "Test1@test.com"},
                new QuestionDTO { Id = 2, Email = "Test2@test.com"}
            };
        }

        private async Task<QuestionDTO> GetQuestionDTOAsync()
        {
            return new QuestionDTO { Id = 1, Email = "Test1@test.com" };
        }

        private async Task<bool> GetTrueAsync()
        {
            return true;
        }

        private async Task<bool> GetFasleAsync()
        {
            return false;
        }

        private async Task<CustomerQuestion> GetNullAsync()
        {
            return null;
        }
        #endregion
    }
}
