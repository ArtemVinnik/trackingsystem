﻿using AutoMapper;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Services;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;
using Xunit;

namespace TrackingSystem.Tests.BLL
{
    public class TaskServiceTests
    {
        private readonly Mock<IUnitOfWork> mockIoW = new Mock<IUnitOfWork>();
        private readonly IMapper mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<TaskDTO, WorkTask>().ReverseMap()));
        
        [Fact]
        public async Task TaskService_GetAllTasksAsync_CollectionOfTasks()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.GetAllAsync()).Returns(GetAllTasksAsync());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.GetAllTasksDTOAsync();

            // Assert
            Assert.Equal((await GetAllTaskDTOsAsync()).Count(), actualResult.Count());
            Assert.Equal(actualResult.ToList()[0].Id, (await GetAllTaskDTOsAsync()).ToList()[0].Id);
        }

        [Fact]
        public void TaskService_GetAllTasks_CollectionOfTasks()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.GetAll()).Returns(GetAllTasks());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = service.GetAllTasksDTO();

            // Assert
            Assert.Equal(GetAllTaskDTOs().Count(), actualResult.Count());
            Assert.Equal(actualResult.ToList()[0].Id, GetAllTaskDTOs().ToList()[0].Id);
        }

        [Fact]
        public async Task TaskService_GetTaskDTOByIdAsync_TaskDTO()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.GetByIdAsync(1)).Returns(GetTaskAsync());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act 
            var actualResult = await service.GetTaskDTOByIdAsync(1);

            // Assert
            Assert.NotNull(actualResult);
            Assert.Equal(actualResult.Id, (await GetTaskDTOAsync()).Id);
        }

        [Fact]
        public async Task TaskService_GetTaskDTOByIdAsync_NULL()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.GetByIdAsync(It.IsAny<int>())).Returns(GetNullAsync());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act 
            var actualResult = await service.GetTaskDTOByIdAsync(It.IsAny<int>());

            // Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task TaskService_UpdateAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.Update(It.IsAny<WorkTask>())).Returns(true);
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.UpdateAsync(It.IsAny<TaskDTO>());

            // Assert
            Assert.True(actualResult.Succeeded);
            Assert.Equal("Updated successfully!", actualResult.Message);
        }

        [Fact]
        public async Task TaskService_UpdateAsync_OperationDetailsNotSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.Update(It.IsAny<WorkTask>())).Returns(false);
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.UpdateAsync(It.IsAny<TaskDTO>());

            // Assert
            Assert.True(!actualResult.Succeeded);
            Assert.Equal("Task doesn`t exist.", actualResult.Message);
        }

        [Fact]
        public void TaskService_Update_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.Update(It.IsAny<WorkTask>())).Returns(true);
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = service.Update(It.IsAny<TaskDTO>());

            // Assert
            Assert.True(actualResult.Succeeded);
            Assert.Equal("Updated successfully!", actualResult.Message);
        }

        [Fact]
        public void TaskService_Update_OperationDetailsNotSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.Update(It.IsAny<WorkTask>())).Returns(false);
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = service.Update(It.IsAny<TaskDTO>());

            // Assert
            Assert.True(!actualResult.Succeeded);
            Assert.Equal("Task doesn`t exist.", actualResult.Message);
        }

        [Fact]
        public async Task TaskService_DeleteAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.DeleteAsync(It.IsAny<int>())).Returns(GetTrueAsync());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.DeleteAsync(It.IsAny<int>());

            // Assert
            Assert.True(actualResult.Succeeded);
            Assert.Equal("Deleted successfully!", actualResult.Message);
        }

        [Fact]
        public async Task TaskService_DeleteAsync_OperationDetailsNotSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.DeleteAsync(It.IsAny<int>())).Returns(GetFasleAsync());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.DeleteAsync(It.IsAny<int>());

            // Assert
            Assert.True(!actualResult.Succeeded);
            Assert.Equal("Task doesn`t exist.", actualResult.Message);
        }

        [Fact]
        public async Task TaskService_CreateAsync_OperationDetailsSuccessful()
        {
            // Arange
            mockIoW.Setup(IoW => IoW.TaskRepository.Create(It.IsAny<WorkTask>())).Returns(It.IsAny<WorkTask>());
            var service = new TaskService(mockIoW.Object, mapper);

            // Act
            var actualResult = await service.CreateAsync(It.IsAny<TaskDTO>());

            // Assert
            Assert.True(actualResult.Succeeded);
        }

        #region Helper methods

        private async Task<IEnumerable<WorkTask>> GetAllTasksAsync()
        {
            return new List<WorkTask>
            {
                new WorkTask { Id = 1, Topic ="Topic1"},
                new WorkTask { Id = 2, Topic = "Topic2"}
            };
        }

        private async Task<WorkTask> GetTaskAsync()
        {
            return new WorkTask { Id = 1, Topic = "Topic1" };
        }

        private async Task<IEnumerable<TaskDTO>> GetAllTaskDTOsAsync()
        {
            return new List<TaskDTO>
            {
                new TaskDTO { Id = 1, Topic ="Topic1"},
                new TaskDTO { Id = 2, Topic = "Topic2"}
            };
        }

        private IEnumerable<WorkTask> GetAllTasks()
        {
            return new List<WorkTask>
            {
                new WorkTask { Id = 1, Topic ="Topic1"},
                new WorkTask { Id = 2, Topic = "Topic2"}
            };
        }

        private IEnumerable<TaskDTO> GetAllTaskDTOs()
        {
            return new List<TaskDTO>
            {
                new TaskDTO { Id = 1, Topic ="Topic1"},
                new TaskDTO { Id = 2, Topic = "Topic2"}
            };
        }

        private async Task<TaskDTO> GetTaskDTOAsync()
        {
            return new TaskDTO { Id = 1, Topic = "Topic1" };
        }

        private async Task<bool> GetTrueAsync()
        {
            return true;
        }

        private async Task<bool> GetFasleAsync()
        {
            return false;
        }

        private async Task<WorkTask> GetNullAsync()
        {
            return null;
        }
        #endregion
    }
}
