﻿using Ninject.Modules;
using TrackingSystemBLL.Interfaces;
using TrackingSystemBLL.Services;
using TrackingSystemDAL.Interfaces;
using TrackingSystemDAL.Repositories;

namespace TrackingSystemBLL.Infrastructure
{
    /// <summary>
    /// Service that implement ninject module.
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;

        /// <summary>
        /// Constructor of class.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public ServiceModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Load binds.
        /// </summary>
        public override void Load()
        {
            Bind<IEmployeeService>().To<EmployyService>();
            Bind<ITaskService>().To<TaskService>();
            Bind<IRoleService>().To<RoleService>();
            Bind<IEmailService>().To<EmailService>();
            Bind<IQuestionService>().To<QuestionService>();

            Bind<IUnitOfWork>().To<IdentityUnitOfWork>().WithConstructorArgument(_connectionString);
        }
    }
}
