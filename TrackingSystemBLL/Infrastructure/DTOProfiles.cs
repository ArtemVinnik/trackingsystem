﻿using AutoMapper;
using TrackingSystemBLL.DTO;
using TrackingSystemDAL.Entities;

namespace TrackingSystemBLL.Infrastructure
{
    /// <summary>
    /// Configuring AutoMapper.
    /// </summary>
    public class DTOProfiles : Profile
    {
        /// <summary>
        /// Constructor of class, that create maps.
        /// </summary>
        public DTOProfiles()
        {
            CreateMap<ApplicationRole, RoleDTO>()
                .ReverseMap();

            CreateMap<ApplicationEmployee, EmployeeDTO>()
                .ForMember("Name", u => u.MapFrom(src => src.EmployeeProfile.Name))
                .ForMember("Surname", u => u.MapFrom(src => src.EmployeeProfile.Surname))
                .ForMember("DateOfBirth", u => u.MapFrom(src => src.EmployeeProfile.DateOfBirth))
                .ReverseMap();

            CreateMap<EmployeeProfile, EmployeeDTO>()
                .ReverseMap();

            CreateMap<WorkTask, TaskDTO>()
                .ForMember("EmployeeId", t => t.MapFrom(src => src.EmployeeProfileId))
                .ReverseMap();

            CreateMap<CustomerQuestion, QuestionDTO>().ReverseMap();
        }
    }
}
