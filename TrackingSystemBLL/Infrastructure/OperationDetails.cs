﻿namespace TrackingSystemBLL.Infrastructure
{
    /// <summary>
    /// The class stores information about the success of the operations.
    /// </summary>
    public class OperationDetails
    {
        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="succeeded">Indicates if the operation was successful.</param>
        /// <param name="message">Message about exception.</param>
        /// <param name="prop">Property that throws the exception.</param>
        public OperationDetails(bool succeeded, string message, string prop)
        {
            Succeeded = succeeded;
            Message = message;
            Property = prop;
        }

        /// <summary>
        /// Indicates if the operation was successful.
        /// </summary>
        public bool Succeeded { get; private set; }

        /// <summary>
        /// Message about exception.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Property that throws the exception.
        /// </summary>
        public string Property { get; private set; }
    }
}
