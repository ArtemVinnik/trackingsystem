﻿using System;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;

namespace TrackingSystemBLL.Interfaces
{
    /// <summary>
    /// Iterface of email service
    /// </summary>
    public interface IEmailService : IDisposable
    {
        /// <summary>
        /// Send information about task.
        /// </summary>
        /// <param name="task">Task instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        Task SendAsync(TaskDTO task, string message);

        /// <summary>
        /// Send customer question to admin.
        /// </summary>
        /// <param name="question">Qustion instanse.</param>
        /// <returns>Nothing</returns>
        Task SendAsync(QuestionDTO question);

        /// <summary>
        /// Send information about task.
        /// </summary>
        /// <param name="task">Task instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        void Send(TaskDTO task, string message);

        /// <summary>
        /// Send information about account.
        /// </summary>
        /// <param name="employee">Employee instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        void Send(EmployeeDTO employee, string message);
    }
}
