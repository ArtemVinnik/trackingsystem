﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;

namespace TrackingSystemBLL.Interfaces
{
    /// <summary>
    /// Question service.
    /// </summary>
    public interface IQuestionService : IDisposable
    {
        /// <summary>
        /// Create question.
        /// </summary>
        /// <param name="questionDTO">QuestionDTO instance.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> CreateAsync(QuestionDTO questionDTO);

        /// <summary>
        /// Delete question from database.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> DeleteAsync(int id);

        /// <summary>
        /// Get all questions.
        /// </summary>
        /// <returns>Collection of QuestionDTO</returns>
        Task<IEnumerable<QuestionDTO>> GetAllQuestionsDTOAsync();

        /// <summary>
        /// Get question.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>QuestionDTO</returns>
        Task<QuestionDTO> GetQuestionDTOByIdAsync(int id);

        /// <summary>
        /// Update question.
        /// </summary>
        /// <param name="questionDTO">QuestionDTO instance.</param>
        /// <returns>Information about updatting.</returns>
        Task<OperationDetails> UpdateAsync(QuestionDTO questionDTO);
    }
}
