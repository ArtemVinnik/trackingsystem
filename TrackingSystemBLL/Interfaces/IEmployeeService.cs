﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;

namespace TrackingSystemBLL.Interfaces
{
    /// <summary>
    /// Employee services.
    /// </summary>
    public interface IEmployeeService : IDisposable
    {
        /// <summary>
        /// Create employee.
        /// </summary>
        /// <param name="employeeDto">EmployeeDTO instance.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> CreateAsync(EmployeeDTO employeeDto);

        /// <summary>
        /// Authenticate employee.
        /// </summary>
        /// <param name="employeeDto">EmployeeDTO instance.</param>
        /// <returns>Information about employee`s authorization.</returns>
        Task<ClaimsIdentity> AuthenticateAsync(EmployeeDTO employeeDto);

        /// <summary>
        /// Delete employee from database.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> DeleteAsync(string id);

        /// <summary>
        /// Get all employees.
        /// </summary>
        /// <returns>Collection of EmployeeDTO</returns>
        Task<IEnumerable<EmployeeDTO>> GetAllEmployeesDTOAsync();

        /// <summary>
        /// Get employee.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>EmployeeDTO</returns>
        Task<EmployeeDTO> GetEmployeeDTOByIdAsync(string id);

        /// <summary>
        /// Get employee.
        /// </summary>
        /// <param name="userName">Nickname.</param>
        /// <returns>EmployeeDTO</returns>
        Task<EmployeeDTO> GetEmployeeDTOByUserNameAsync(string userName);

        /// <summary>
        /// Update information about emploee.
        /// </summary>
        /// <param name="employeeDto">EmployeeDTO instance.</param>
        /// <returns>Information about employee`s authorization.</returns>
        Task<OperationDetails> UpdateAsync(EmployeeDTO employeeDto);

        /// <summary>
        /// Generate password reset token async.
        /// </summary>
        /// <param name="id">User id.</param>
        /// <returns>Reset token.</returns>
        Task<string> GeneratePasswordResetTokenAsync(string id);

        /// <summary>
        /// Reset password of employee.
        /// </summary>
        /// <param name="id">Id of imployee.</param>
        /// <param name="token">reset token.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> ResetPasswordAsync(string id, string token, string newPassword);
    }
}
