﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;

namespace TrackingSystemBLL.Interfaces
{
    /// <summary>
    /// Task service.
    /// </summary>
    public interface ITaskService : IDisposable
    {
        /// <summary>
        /// Create task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> CreateAsync(TaskDTO taskDTO);

        /// <summary>
        /// Delete task from database.
        /// </summary>
        /// <param name="id">Id of task.</param>
        /// <returns>Information about operation.</returns>
        Task<OperationDetails> DeleteAsync(int id);

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of TaskDTO</returns>
        Task<IEnumerable<TaskDTO>> GetAllTasksDTOAsync();

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of TaskDTO</returns>
        IEnumerable<TaskDTO> GetAllTasksDTO();

        /// <summary>
        /// Get task.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>EmployeeDTO</returns>
        Task<TaskDTO> GetTaskDTOByIdAsync(int id);

        Task<int> GetIdOfTaskByEmployeeIdAsync(string id);

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about updatting.</returns>
        Task<OperationDetails> UpdateAsync(TaskDTO taskDTO);

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about updatting.</returns>
        OperationDetails Update(TaskDTO taskDTO);
    }
}
