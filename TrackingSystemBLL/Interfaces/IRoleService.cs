﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;

namespace TrackingSystemBLL.Interfaces
{
    /// <summary>
    /// Role service.
    /// </summary>
    public interface IRoleService : IDisposable
    {
        /// <summary>
        /// Create role.
        /// </summary>
        /// <param name="roleDTO">Role instance.</param>
        /// <returns>Operation details.</returns>
        Task<OperationDetails> CreateAsync(RoleDTO roleDTO);

        /// <summary>
        /// Delete role.
        /// </summary>
        /// <param name="id">Role id.</param>
        /// <returns>Operation details.</returns>
        Task<OperationDetails> DeleteAsync(string id);

        /// <summary>
        /// Get all roles.
        /// </summary>
        /// <returns>Collection of roles.</returns>
        Task<IEnumerable<RoleDTO>> GetAllRolesAsync();

        /// <summary>
        /// Get role by id.
        /// </summary>
        /// <param name="id">Role id.</param>
        /// <returns>Role instance.</returns>
        Task<RoleDTO> GetRoleByIdAsync(string id);

        /// <summary>
        /// Update role.
        /// </summary>
        /// <param name="roleDTO">Role instance.</param>
        /// <returns>Operation details.</returns>
        Task<OperationDetails> UpdateAsync(RoleDTO roleDTO);

        /// <summary>
        /// Get role by name.
        /// </summary>
        /// <param name="name">Role name.</param>
        /// <returns>Role instance.</returns>
        Task<RoleDTO> GetRoleByNameAsync(string name);
    }
}
