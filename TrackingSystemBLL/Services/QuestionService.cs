﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;
using TrackingSystemBLL.Interfaces;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemBLL.Services
{
    /// <summary>
    /// Question service.
    /// </summary>
    public class QuestionService : IQuestionService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="database">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public QuestionService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create question.
        /// </summary>
        /// <param name="questionDTO">QuestionDTO instance.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> CreateAsync(QuestionDTO questionDTO)
        {
            var question = _mapper.Map<CustomerQuestion>(questionDTO);
            _database.QuestionRepository.Create(question);

            await _database.SaveAsync();

            return new OperationDetails(true, "", "");
        }

        /// <summary>
        /// Delete question from database.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> DeleteAsync(int id)
        {
            var result = await _database.QuestionRepository.DeleteAsync(id);
            await _database.SaveAsync();
            if (result)
            {
                return new OperationDetails(true, "Deleted successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "Message doesn`t exist.", "");
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Get all questions.
        /// </summary>
        /// <returns>Collection of QuestionDTO</returns>
        public async Task<IEnumerable<QuestionDTO>> GetAllQuestionsDTOAsync()
        {
            return _mapper.Map<IEnumerable<QuestionDTO>>(await _database.QuestionRepository.GetAllAsync());
        }

        /// <summary>
        /// Get question.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>QuestionDTO</returns>
        public async Task<QuestionDTO> GetQuestionDTOByIdAsync(int id)
        {
            return _mapper.Map<QuestionDTO>(await _database.QuestionRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Update question.
        /// </summary>
        /// <param name="questionDTO">QuestionDTO instance.</param>
        /// <returns>Information about updatting.</returns>
        public async Task<OperationDetails> UpdateAsync(QuestionDTO questionDTO)
        {
            var result = _database.QuestionRepository.Update(_mapper.Map<CustomerQuestion>(questionDTO));
            if (result)
            {
                await _database.SaveAsync();
                return new OperationDetails(true, "Updated successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "Message doesn`t exist.", "");
            }
        }
    }
}
