﻿using AutoMapper;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;
using TrackingSystemBLL.Interfaces;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemBLL.Services
{
    /// <summary>
    /// Role service.
    /// </summary>
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="database">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public RoleService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create role.
        /// </summary>
        /// <param name="roleDTO">Role instance.</param>
        /// <returns>Operation details.</returns>
        public async Task<OperationDetails> CreateAsync(RoleDTO roleDTO)
        {
            var role = await _database.RoleManager.FindByNameAsync(roleDTO.Name.ToLower());

            if (role == null)
            {
                var id = new ApplicationRole().Id;

                roleDTO.Name = roleDTO.Name.ToLower();

                role = _mapper.Map<ApplicationRole>(roleDTO);
                role.Id = id;

                var result = await _database.RoleManager.CreateAsync(role);

                if (result.Errors.Count() > 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }

                await _database.SaveAsync();

                return new OperationDetails(true, "Role created successfully", "");
            }
            {
                return new OperationDetails(false, "Role with such name exist", "");
            }
        }

        /// <summary>
        /// Delete role.
        /// </summary>
        /// <param name="id">Role id.</param>
        /// <returns>Operation details.</returns>
        public async Task<OperationDetails> DeleteAsync(string id)
        {
            var role = await _database.RoleManager.FindByIdAsync(id);

            if(role != null)
            {
                var result =  await _database.RoleManager.DeleteAsync(role);

                if (result.Errors.Count() > 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }

                await _database.SaveAsync();

                return new OperationDetails(true, "Role deleted successfully", "");
            }
            {
                return new OperationDetails(false, "Role doesn`t exist with such id", "");
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Get all roles.
        /// </summary>
        /// <returns>Collection of roles.</returns>
        public async Task<IEnumerable<RoleDTO>> GetAllRolesAsync()
        {
            return _mapper.Map<IEnumerable<RoleDTO>>(await _database.RoleManager.Roles.ToListAsync());
        }

        /// <summary>
        /// Get role by id.
        /// </summary>
        /// <param name="id">Role id.</param>
        /// <returns>Role instance.</returns>
        public async Task<RoleDTO> GetRoleByIdAsync(string id)
        {
            return _mapper.Map<RoleDTO>(await _database.RoleManager.FindByIdAsync(id));
        }

        /// <summary>
        /// Get role by name.
        /// </summary>
        /// <param name="name">Role name.</param>
        /// <returns>Role instance.</returns>
        public async Task<RoleDTO> GetRoleByNameAsync(string name)
        {
            return _mapper.Map<RoleDTO>(await _database.RoleManager.FindByNameAsync(name));
        }

        /// <summary>
        /// Update role.
        /// </summary>
        /// <param name="roleDTO">Role instance.</param>
        /// <returns>Operation details.</returns>
        public async Task<OperationDetails> UpdateAsync(RoleDTO roleDTO)
        {
            var role = await _database.RoleManager.FindByIdAsync(roleDTO.Id);

            if (role != null)
            {
                role.Id = null;

                var result = await _database.RoleManager.UpdateAsync(role);

                if (result.Errors.Count() > 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }

                await _database.SaveAsync();

                return new OperationDetails(true, "Role deleted successfully", "");
            }
            {
                return new OperationDetails(false, "Role doesn`t exist with such id", "");
            }
        }
    }
}
