﻿using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Interfaces;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemBLL.Services
{
    /// <summary>
    /// Email service.
    /// </summary>
    public class EmailService : IEmailService
    {
        private readonly string projectEmail = ConfigurationManager.AppSettings.Get("email");
        private readonly string projectPassword = ConfigurationManager.AppSettings.Get("password");

        private readonly IUnitOfWork _database;

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="database">Unit of work.</param>
        public EmailService(IUnitOfWork database)
        {
            _database = database;
        }

        /// <summary>
        /// Send information about task.
        /// </summary>
        /// <param name="task">Task instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        public async Task SendAsync(TaskDTO task, string message) 
        {
            var employee = await _database.EmployeeManager.FindByIdAsync(task.EmployeeId);

            MailAddress from = new MailAddress(projectEmail);
            MailAddress to = new MailAddress(employee.Email);
            MailMessage m = new MailMessage(from, to);

            m.Subject = "Information about the task.";
            m.Body = message;
            m.IsBodyHtml = true;

            SendSmtp(m);
        }

        /// <summary>
        /// Send information about account.
        /// </summary>
        /// <param name="employee">Employee instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        public void Send(EmployeeDTO employee, string message)
        {
            MailAddress from = new MailAddress(projectEmail);
            MailAddress to = new MailAddress(employee.Email);
            MailMessage m = new MailMessage(from, to);

            m.Subject = "Information about the account.";
            m.Body = message;
            m.IsBodyHtml = true;

            SendSmtp(m);
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Send information about task.
        /// </summary>
        /// <param name="task">Task instance.</param>
        /// <param name="message">Text of message.</param>
        /// <returns>Nothing</returns>
        public void Send(TaskDTO task, string message)
        {
            var employee = _database.EmployeeManager.FindByIdAsync(task.EmployeeId);

            MailAddress from = new MailAddress(projectEmail);
            MailAddress to = new MailAddress(employee.Result.Email);
            MailMessage m = new MailMessage(from, to);

            m.Subject = "Information about the task.";
            m.Body = message;
            m.IsBodyHtml = false;

            SendSmtp(m);
        }

        /// <summary>
        /// Send customer question to admin.
        /// </summary>
        /// <param name="question">Qustion instanse.</param>
        /// <returns>Nothing</returns>
        public async Task SendAsync(QuestionDTO question)
        {
            var adminRoleId = (await _database.RoleManager.FindByNameAsync("admin")).Id;

            var admins = _database.EmployeeManager.Users.Where(u => u.Roles.FirstOrDefault(r => r.RoleId == adminRoleId) != null);

            foreach (var admin in admins)
            {
                MailAddress from = new MailAddress(projectEmail);
                MailAddress to = new MailAddress(admin.Email);
                MailMessage m = new MailMessage(from, to);

                m.Subject = question.Subject != null ? question.Subject : "";

                var name = question.Name != null ? question.Name : "none";
                StringBuilder message = new StringBuilder($"Hellow, my name {name}.\n");
                message.AppendLine($"Email: {question.Email}");
                message.AppendLine($"{question.Message}");

                m.Body = message.ToString();
                m.IsBodyHtml = false;

                SendSmtp(m);
            }
        }

        private void SendSmtp(MailMessage mail)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential(projectEmail, projectPassword);
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}
