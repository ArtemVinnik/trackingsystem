﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;
using TrackingSystemBLL.Interfaces;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemBLL.Services
{
    /// <summary>
    /// User services.
    /// </summary>
    public class EmployyService : IEmployeeService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="database">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public EmployyService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Authenticate user.
        /// </summary>
        /// <param name="employeeDto">userDTO instance.</param>
        /// <returns>Information about user`s authorization.</returns>
        public async Task<ClaimsIdentity> AuthenticateAsync(EmployeeDTO employeeDto)
        {
            ClaimsIdentity claim = null;

            var employee = await _database.EmployeeManager.FindAsync(employeeDto.Email, employeeDto.Password);
            if (employee != null)
            {
                claim = await _database.EmployeeManager.CreateIdentityAsync(employee, DefaultAuthenticationTypes.ApplicationCookie);
            }

            return claim;
        }

        /// <summary>
        /// Create user.
        /// </summary>
        /// <param name="employeeDto">userDTO instance.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> CreateAsync(EmployeeDTO employeeDto)
        {
            var employee = await _database.EmployeeManager.FindByEmailAsync(employeeDto.Email);

            if (employee == null)
            {
                var id = new ApplicationEmployee().Id;

                employee = _mapper.Map<ApplicationEmployee>(employeeDto);
                employee.Id = id;
                var employeeProfile = _mapper.Map<EmployeeProfile>(employeeDto);
                employeeProfile.Id = id;

                var result = await _database.EmployeeManager.CreateAsync(employee, employeeDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                if (employeeDto.RoleId != null)
                {
                    await _database.EmployeeManager.AddToRoleAsync(employee.Id, (await _database.RoleManager.FindByIdAsync(employeeDto.RoleId)).Name);
                }
                else
                {
                    await _database.EmployeeManager.AddToRoleAsync(employee.Id, "employee");
                }

                _database.EmployeeProfileManager.Create(employeeProfile);
                await _database.SaveAsync();

                return new OperationDetails(true, "Registration completed successfully.", "");
            }
            else
            {
                return new OperationDetails(false, "Employee exist with such email.", "Email");
            }
        }

        /// <summary>
        /// Delete employee from database.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> DeleteAsync(string id)
        {
            var output =  await _database.EmployeeProfileManager.DeleteAsync(id);
            if (output != false)
            {
                var employee = await _database.EmployeeManager.FindByIdAsync(id);

                var result =  await _database.EmployeeManager.DeleteAsync(employee);

                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                return new OperationDetails(true, "Deleting completed successfully.", "");
            }
            else
            {
                return new OperationDetails(false, "Employee doesn`t exist with such id.", "");
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Get employee.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>EmployeeDTO</returns>
        public async Task<EmployeeDTO> GetEmployeeDTOByIdAsync(string id)
        {
            return _mapper.Map<EmployeeDTO>(await _database.EmployeeManager.FindByIdAsync(id));
        }

        /// <summary>
        /// Get all employees.
        /// </summary>
        /// <returns>Collection of EmployeeDTO</returns>
        public async Task<IEnumerable<EmployeeDTO>> GetAllEmployeesDTOAsync()
        {
            return _mapper.Map<IEnumerable<EmployeeDTO>>(await _database.EmployeeManager.Users.ToListAsync());
        }

        /// <summary>
        /// Update information about emploee.
        /// </summary>
        /// <param name="employeeDto">EmployeeDTO instance.</param>
        /// <returns>Information about employee`s authorization.</returns>
        public async Task<OperationDetails> UpdateAsync(EmployeeDTO employeeDto)
        {
            var employee = await _database.EmployeeManager.FindByIdAsync(employeeDto.Id);

            if (employee != null)
            {
                employee = _mapper.Map<ApplicationEmployee>(employeeDto);
                employee.Id = null;
                var employeeProfile = _mapper.Map<EmployeeProfile>(employeeDto);

                var profileResult = _database.EmployeeProfileManager.Update(employeeProfile);

                var identityResult = await _database.EmployeeManager.UpdateAsync(employee);

                var result = identityResult.Errors.Count() > 0 && profileResult == false;

                if (result)
                    return new OperationDetails(false, identityResult.Errors.FirstOrDefault() + "\nUpdating didn`t complete successfully.", "");

                await _database.SaveAsync();

                return new OperationDetails(true, "Updating completed successfully.", "");
            }
            else
            {
                return new OperationDetails(false, "Employee doesn`t exist.", "");
            }
        }

        /// <summary>
        /// Get employee.
        /// </summary>
        /// <param name="userName">Nickname.</param>
        /// <returns>EmployeeDTO</returns>
        public async Task<EmployeeDTO> GetEmployeeDTOByUserNameAsync(string userName)
        {
            return _mapper.Map<EmployeeDTO>(await _database.EmployeeManager.FindByNameAsync(userName));
        }

        /// <summary>
        /// Generate password reset token async.
        /// </summary>
        /// <param name="id">User id.</param>
        /// <returns>Reset token.</returns>
        public async Task<string> GeneratePasswordResetTokenAsync(string id)
        {
            return await _database.EmployeeManager.GeneratePasswordResetTokenAsync(id);
        }

        /// <summary>
        /// Reset password of employee.
        /// </summary>
        /// <param name="id">Id of imployee.</param>
        /// <param name="token">reset token.</param>
        /// <param name="newPassword">New password.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> ResetPasswordAsync(string id, string token, string newPassword)
        {
            var result = await _database.EmployeeManager.ResetPasswordAsync(id, token, newPassword);

            if (result.Errors.Count() > 0)
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

            await _database.SaveAsync();

            return new OperationDetails(true, "Updating completed successfully.", "");
        }
    }
}
