﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Infrastructure;
using TrackingSystemBLL.Interfaces;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemBLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="database">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public TaskService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> CreateAsync(TaskDTO taskDTO)
        {
            var task = _mapper.Map<WorkTask>(taskDTO);
            _database.TaskRepository.Create(task);
            await _database.SaveAsync();
            return new OperationDetails(true, "", "");
        }

        /// <summary>
        /// Delete task from database.
        /// </summary>
        /// <param name="id">Id of task.</param>
        /// <returns>Information about operation.</returns>
        public async Task<OperationDetails> DeleteAsync(int id)
        {
            var result = await _database.TaskRepository.DeleteAsync(id);
            await _database.SaveAsync();
            if (result)
            {
                return new OperationDetails(true, "Deleted successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "Task doesn`t exist.", "");
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of EmployeeDTO</returns>
        public async Task<IEnumerable<TaskDTO>> GetAllTasksDTOAsync()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(await _database.TaskRepository.GetAllAsync());
        }

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of EmployeeDTO</returns>
        public IEnumerable<TaskDTO> GetAllTasksDTO()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_database.TaskRepository.GetAll());
        }

        /// <summary>
        /// Get task.
        /// </summary>
        /// <param name="id">Id of employee.</param>
        /// <returns>EmployeeDTO</returns>
        public async Task<TaskDTO> GetTaskDTOByIdAsync(int id)
        {
            return _mapper.Map<TaskDTO>(await _database.TaskRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about employee`s authorization.</returns>
        public async Task<OperationDetails> UpdateAsync(TaskDTO taskDTO)
        {
            var result = _database.TaskRepository.Update(_mapper.Map<WorkTask>(taskDTO));
            if (result)
            {
                await _database.SaveAsync();
                return new OperationDetails(true, "Updated successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "Task doesn`t exist.", "");
            }
        }

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="taskDTO">TaskDTO instance.</param>
        /// <returns>Information about employee`s authorization.</returns>
        public OperationDetails Update(TaskDTO taskDTO)
        {
            var result = _database.TaskRepository.Update(_mapper.Map<WorkTask>(taskDTO));
            if (result)
            {
                _database.Save();
                return new OperationDetails(true, "Updated successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "Task doesn`t exist.", "");
            }
        }

        public async Task<int> GetIdOfTaskByEmployeeIdAsync(string id)
        {
            var taskId = (await _database.TaskRepository.GetAllAsync()).Where(t => t.EmployeeProfileId == id).OrderByDescending(t => t.Creating).Select(t => t.Id);

            return taskId.ToList()[0];
        }
    }
}
