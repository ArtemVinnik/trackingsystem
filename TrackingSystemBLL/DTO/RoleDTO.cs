﻿namespace TrackingSystemBLL.DTO
{
    /// <summary>
    /// Data transfer class for role.
    /// </summary>
    public class RoleDTO
    {
        /// <summary>
        /// Id of the role.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name of the role.
        /// </summary>
        public string Name { get; set; }
    }
}
