﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TrackingSystemBLL.DTO
{
    /// <summary>
    /// Data transfer class for employee.
    /// </summary>
    public class EmployeeDTO
    {
        /// <summary>
        /// Id.
        /// </summary>
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8)]
        public string Password { get; set; }

        /// <summary>
        /// Nickname
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        /// Employee name.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your name should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Employee surname.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your surname should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Surname { get; set; }

        /// <summary>
        /// Employee date of Birth.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// EmployeeDTO role.
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Collection of role`s id.
        /// </summary>
        public ICollection<int> WorkTaskId { get; set; }
    }
}
