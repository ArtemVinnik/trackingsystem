﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrackingSystemBLL.DTO
{
    /// <summary>
    /// Data transfer object for questions.
    /// </summary>
    public class QuestionDTO
    {
        /// <summary>
        /// Id of question.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Name of custoner
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email of custoner
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Subject of message
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [Required]
        public string Message { get; set; }

        /// <summary>
        /// Viewed
        /// </summary>
        [Required]
        public bool Viewed { get; set; }
    }
}
