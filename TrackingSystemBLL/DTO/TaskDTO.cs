﻿using System;

namespace TrackingSystemBLL.DTO
{
    /// <summary>
    /// Data transfer object for tasks.
    /// </summary>
    public class TaskDTO
    {
        /// <summary>
        /// Id of task.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Topic
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Instruction to task.
        /// </summary>
        public string Instruction { get; set; }

        /// <summary>
        /// Date of creating.
        /// </summary
        public DateTime Creating { get; set; }

        /// <summary>
        /// Deadline.
        /// </summary>
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Time when it was handed over.
        /// </summary>
        public DateTime Expired { get; set; }

        /// <summary>
        /// Property for soft deleting.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Employee id.
        /// </summary>
        public string EmployeeId { get; set; }
    }
}
