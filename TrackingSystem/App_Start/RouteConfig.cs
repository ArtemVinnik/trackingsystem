﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TrackingSystem
{
    /// <summary>
    ///  Used to register various route patterns for your Asp.Net MVC application.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Setting routes.
        /// </summary>
        /// <param name="routes">Reotes collection.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
