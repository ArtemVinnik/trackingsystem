﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Ninject;
using Ninject.Modules;
using Owin;
using System.Configuration;
using TrackingSystem.Infrastructure;
using TrackingSystemBLL.Infrastructure;
using TrackingSystemBLL.Interfaces;

[assembly: OwinStartup(typeof(TrackingSystem.App_Start.Startup))]

namespace TrackingSystem.App_Start
{
    public class Startup
    {
        private IKernel kernel = null;

        public void Configuration(IAppBuilder app)
        {
            kernel = CreateKernel();
            app.UseNinject(() => kernel);

            app.CreatePerOwinContext(() => kernel.Get<IEmployeeService>());
            app.CreatePerOwinContext(() => kernel.Get<ITaskService>());
            app.CreatePerOwinContext(() => kernel.Get<IRoleService>());
            app.CreatePerOwinContext(() => kernel.Get<IEmailService>());
            app.CreatePerOwinContext(() => kernel.Get<IQuestionService>());

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Login")
            });
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["TrackingSystemConnection"].ConnectionString;

                NinjectModule dependencyResolverModule = new DependencyResolverModule();
                NinjectModule autoMapperModule = new AutoMapperModule();
                NinjectModule serviceModule = new ServiceModule(connectionString);

                kernel.Load(dependencyResolverModule, serviceModule, autoMapperModule);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }
    }
}
