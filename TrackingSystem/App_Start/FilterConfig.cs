﻿using System.Web.Mvc;

namespace TrackingSystem
{
    /// <summary>
    /// Used to create and register global MVC filter error filter.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Registering global filters into the filters collection.
        /// </summary>
        /// <param name="filters">Filters collection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
