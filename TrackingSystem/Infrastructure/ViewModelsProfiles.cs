﻿using AutoMapper;
using TrackingSystem.Enums;
using TrackingSystem.ViewModels;
using TrackingSystemBLL.DTO;

namespace TrackingSystem.Infrastructure
{
    /// <summary>
    /// Configuring AutoMapper.
    /// </summary>
    public class ViewModelsProfiles : Profile
    {
        /// <summary>
        /// Constructor of class, that create maps.
        /// </summary>
        public ViewModelsProfiles()
        {
            CreateMap<EmployeeDTO, LoginModel>()
                .ReverseMap();

            CreateMap<EmployeeDTO, RegisterModel>()
                .ReverseMap();

            CreateMap<EmployeeDTO, DisplayEmployeeModel>()
                .ForMember(devm => devm.FullName, edto => edto.MapFrom(p => p.Name + " " + p.Surname))
                .ReverseMap();

            CreateMap<EmployeeDTO, UpdateProfile>().ReverseMap();

            CreateMap<CreateTask, TaskDTO>()
                .ForMember(ct => ct.Status, t => t.MapFrom(src => (int)src.Status));

            CreateMap<TaskDTO, CreateTask>()
                .ForMember(t => t.Status, ct => ct.MapFrom(src => (Status)src.Status));

            CreateMap<DisplayTask, TaskDTO>()
                .ForMember(dt => dt.Status, t => t.MapFrom(src => (int)src.Status));

            CreateMap<TaskDTO, DisplayTask>()
                .ForMember(t => t.Status, dt => dt.MapFrom(src => (Status)src.Status));

            CreateMap<TaskDTO, TaskDetails>().ReverseMap();

            CreateMap<QuestionDTO, Feedback>().ReverseMap();
            CreateMap<QuestionDTO, FeedbackDesplay>().ReverseMap();
        }
    }
}