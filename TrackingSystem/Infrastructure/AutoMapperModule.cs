﻿using AutoMapper;
using Ninject;
using Ninject.Modules;
using TrackingSystemBLL.Infrastructure;

namespace TrackingSystem.Infrastructure
{
    /// <summary>
    /// Service that implement ninject module.
    /// </summary>
    public class AutoMapperModule : NinjectModule
    {
        public override void Load()
        {
            var mapperConfiguration = CreateConfiguration();
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();

            Bind<IMapper>().ToMethod(ctx =>
                 new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        }

        /// <summary>
        /// Create configuration for mapper, that adds all maps.
        /// </summary>
        /// <returns>Configuration for mapper.</returns>
        private MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Add all profiles in current assembly
                cfg.AddMaps(typeof(ViewModelsProfiles), typeof(DTOProfiles));
            });

            return config;
        }
    }
}