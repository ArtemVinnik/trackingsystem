﻿using Ninject.Modules;
using System.Web;

namespace TrackingSystem.Infrastructure
{
    /// <summary>
    ///  A loadable unit that defines bindings for my application.
    /// </summary>
    public class DependencyResolverModule : NinjectModule
    {
        /// <summary>
        ///  Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {
            Bind<IHttpModule>().To<TimerModule>();
        }
    }
}