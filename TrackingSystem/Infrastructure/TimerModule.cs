﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using TrackingSystem.Enums;
using TrackingSystemBLL.Interfaces;

namespace TrackingSystem.Infrastructure
{
    /// <summary>
    /// Module for sending messages at a certain time.
    /// </summary>
    public class TimerModule : IHttpModule
    {
        static Timer timer;
        long interval = 30000; 
        static object synclock = new object();

        private readonly ITaskService _taskService;
        private readonly IEmailService _emailService;

        /// <summary>
        /// Constructor of class.
        /// </summary>
        /// <param name="taskService"></param>
        /// <param name="emailService"></param>
        public TimerModule(ITaskService taskService, IEmailService emailService)
        {
            _taskService = taskService;
            _emailService = emailService;
        }

        private IEmailService EmailService
        {
            get
            {
                return _emailService;
            }
        }

        private ITaskService TaskService
        {
            get
            {
                return _taskService;
            }
        }

        /// <summary>
        /// Шnitialization
        /// </summary>
        /// <param name="context">Http context.</param>
        public void Init(HttpApplication context)
        {
            timer = new Timer(new TimerCallback(SendEmail), null, 0, interval);
        }

        /// <summary>
        /// Send mail.
        /// </summary>
        /// <param name="obj">Object instance.</param>
        private void SendEmail(object obj) 
        {
            lock (synclock)
            {
                var tasks = TaskService.GetAllTasksDTO().Where(t => t.IsDeleted == false && (t.Status != (int)Status.Submitted || t.Status != (int)Status.SubmittedLate));

                DateTime dd = DateTime.Now;
                if (dd.Hour == 3 && dd.Minute == 30)
                {
                    foreach (var task in tasks)
                    {
                        if (dd >= task.Deadline && task.Status != (int)Status.Expired && task.Status != (int)Status.Submitted && task.Status != (int)Status.SubmittedLate)
                        {
                            task.Status = (int)Status.Expired;

                            TaskService.Update(task);

                            StringBuilder message = new StringBuilder("\nThe deadline for the task has expired!\n");
                            message.Append($"Topic: {task.Topic}\n");

                            EmailService.Send(task, message.ToString());
                        }
                        else if (dd >= task.Deadline && task.Status == (int)Status.Expired)
                        {
                            StringBuilder message = new StringBuilder("The deadline for the task has expired!\n");
                            message.Append($"Topic: {task.Topic}\n");
                            message.Append("You must to do the task!!!\n");

                            EmailService.Send(task, message.ToString());
                        }
                    }
                }
            }
        }

        public void Dispose() { }
    }
}