﻿namespace TrackingSystem.Enums
{
    /// <summary>
    /// Status of task.
    /// </summary>
    public enum Status
    {
        Issued, // Выдано.
        Updated, // Обнавлено.
        Expired, // Просрочено.
        Submitted, // Сдано.
        SubmittedLate // Сдано с опозданием. 
    }
}
