﻿using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TrackingSystem.ViewModels;
using TrackingSystem.Enums;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Interfaces;
using PagedList;

namespace TrackingSystem.Controllers
{
    /// <summary>
    /// Account controller
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IMapper mapper;

        /// <summary>
        /// Construcntor.
        /// </summary>
        /// <param name="mapper">Mapper</param>
        public AccountController(IMapper mapper)
        {
            this.mapper = mapper;
        }

        private IEmployeeService EmployeeService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmployeeService>();
            }
        }

        private IRoleService RoleService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IRoleService>();
            }
        }

        private IEmailService EmailService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmailService>();
            }
        }

        /// <summary>
        /// https://localhost:44320/Account/Employees
        /// </summary>
        /// <param name="page">Namber of page</param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> Employees(int? page)
        {
            var employees = mapper.Map<IEnumerable<DisplayEmployeeModel>>(await EmployeeService.GetAllEmployeesDTOAsync());

            int pageSize = 8;
            int pageNumber = (page ?? 1);

            return View(employees.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// https://localhost:44320/Account/Registretion
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Registretion()
        {
            ViewBag.Roles = await RoleService.GetAllRolesAsync();

            return View();
        }

        /// <summary>
        /// https://localhost:44320/Account/Registretion
        /// </summary>
        /// <param name="model">Register model.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Registretion(RegisterModel model)
        {
            if (model.DateOfBirth > new DateTime((int)DateTime.Now.Year - 18, (int)DateTime.Now.Month, (int)DateTime.Now.Day))
                ModelState.AddModelError("DateOfBirth", "Employee muste be 18 years old.");

            if (ModelState.IsValid)
            {
                var user = mapper.Map<EmployeeDTO>(model);
                user.UserName = user.Email;
                user.RoleId = (await RoleService.GetRoleByNameAsync(((Roles)Enum.GetValues(typeof(Roles)).GetValue(model.RoleId)).ToString())).Id;

                var operationDetails = await EmployeeService.CreateAsync(user);
                if (operationDetails.Succeeded)
                {
                    EmailService.Send(user, "Your account added to the system");

                    return RedirectToAction("Employees");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }

            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Account/Edit/
        /// </summary>
        /// <param name="id">Employee`s id.</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(string id)
        {
            var employee = await EmployeeService.GetEmployeeDTOByIdAsync(id);

            if (employee == null)
                return RedirectToAction("NotFound", "Error");

            return View(mapper.Map<UpdateProfile>(await EmployeeService.GetEmployeeDTOByIdAsync(id)));
        }

        /// <summary>
        /// https://localhost:44320/Account/Edit/
        /// </summary>
        /// <param name="id">Employee`s id.</param>
        /// <param name="model">Update model.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string id, UpdateProfile model)
        {
            if (model.DateOfBirth > new DateTime((int)DateTime.Now.Year - 18, (int)DateTime.Now.Month, (int)DateTime.Now.Day))
                ModelState.AddModelError("DateOfBirth", "Employee muste be 18 years old.");

            if (ModelState.IsValid)
            {
                var employee = mapper.Map<EmployeeDTO>(model);
                employee.Id = id;
                employee.UserName = model.Email;

                var operationDetails = await EmployeeService.UpdateAsync(employee);
                if (operationDetails.Succeeded)
                {
                    return RedirectToAction("Employees");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }

            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Account/Delete/
        /// </summary>
        /// <param name="id">Employee`s id.</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(string id)
        {
            var employee = await EmployeeService.GetEmployeeDTOByIdAsync(id);

            if (employee == null)
                return RedirectToAction("NotFound", "Error");

            return View(mapper.Map<DisplayEmployeeModel>(await EmployeeService.GetEmployeeDTOByIdAsync(id)));
        }

        /// <summary>
        /// https://localhost:44320/Account/Delete/
        /// </summary>
        /// <param name="id">Employee`s id.</param>
        /// <param name="collection">Form collection.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string id, FormCollection collection)
        {
            await EmployeeService.DeleteAsync(id);

            return RedirectToAction("Employees");
        }
    }
}
