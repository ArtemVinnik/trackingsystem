﻿using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TrackingSystem.ViewModels;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Interfaces;

namespace TrackingSystem.Controllers
{
    /// <summary>
    /// Home controller.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IMapper mapper;

        /// <summary>
        /// Construcntor.
        /// </summary>
        /// <param name="mapper">Mapper</param>
        public HomeController(IMapper mapper)
        {
            this.mapper = mapper;
        }

        private IEmployeeService EmployeeService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmployeeService>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private IEmailService EmailService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmailService>();
            }
        }

        private IQuestionService QuestionService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IQuestionService>();
            }
        }

        /// <summary>
        /// https://localhost:44320/
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            return View();
        }

        /// <summary>
        /// https://localhost:44320/
        /// </summary>
        /// <param name="feedback">Feedback model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                var question = mapper.Map<QuestionDTO>(feedback);
                question.Viewed = false;
                await QuestionService.CreateAsync(question);
                await EmailService.SendAsync(question);

                return RedirectToAction("Index");
            }

            return View(feedback);
        }

        /// <summary>
        /// https://localhost:44320/Home/Login
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// https://localhost:44320/Home/Login
        /// </summary>
        /// <param name="login">Login model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                var user = mapper.Map<EmployeeDTO>(login);
                var claim = await EmployeeService.AuthenticateAsync(user);

                if (claim == null)
                {
                    ModelState.AddModelError("", "Email or password is incorrect.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = login.Remember
                    }, claim);
                    return RedirectToAction("Index");
                }
            }

            return View(login);
        }

        /// <summary>
        /// https://localhost:44320/Home/ForgotPassword
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// https://localhost:44320/Home/ForgotPassword
        /// </summary>
        /// <param name="model">Forgot password model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var employee = await EmployeeService.GetEmployeeDTOByUserNameAsync(model.Email);

                if (User == null)
                {
                    return View("ForgotPasswordConfirmation");
                }

                var code = await EmployeeService.GeneratePasswordResetTokenAsync(employee.Id);
                var callbackUrl = Url.Action("ResetPassword", "Home", new { userId = employee.Id, code = code }, protocol: Request.Url.Scheme);
                EmailService.Send(employee, "Reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

                return RedirectToAction("ForgotPasswordConfirmation", "Home");
            }

            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Home/ForgotPasswordConfirmation
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// https://localhost:44320/Home/ResetPasswordConfirmation
        /// </summary>
        /// <returns></returns>
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// https://localhost:44320/Home/ResetPassword
        /// </summary>
        /// <param name="code">Reset password token.</param>
        /// <returns></returns>
        public ActionResult ResetPassword(string code)
        {
            if (code == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// https://localhost:44320/Home/ResetPassword
        /// </summary>
        /// <param name="model">Reset password model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var employee = await EmployeeService.GetEmployeeDTOByUserNameAsync(model.Email);
            if (employee == null)
            {
                return RedirectToAction("ResetPasswordConfirmation");
            }
            var result = await EmployeeService.ResetPasswordAsync(employee.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation");
            }

            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Home/Logout
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// https://localhost:44320/Home/About
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}