﻿using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TrackingSystem.ViewModels;
using TrackingSystemBLL.Interfaces;

namespace TrackingSystem.Controllers
{
    /// <summary>
    /// Question controller
    /// </summary>
    [Authorize(Roles = "admin")]
    public class QuestionController : Controller
    {
        private readonly IMapper mapper;

        /// <summary>
        /// Construcntor.
        /// </summary>
        /// <param name="mapper">Mapper</param>
        public QuestionController(IMapper mapper)
        {
            this.mapper = mapper;
        }

        private IQuestionService QuestionService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IQuestionService>();
            }
        }

        /// <summary>
        /// https://localhost:44320/Question/Questions
        /// </summary>
        /// <param name="page">Number of page.</param>
        /// <returns></returns>
        public async Task<ActionResult> Questions(int? page)
        {
            var questions = mapper.Map<IEnumerable<FeedbackDesplay>>((await QuestionService.GetAllQuestionsDTOAsync()).OrderByDescending(q => q.Viewed));

            int pageSize = 8;
            int pageNumber = (page ?? 1);

            return View(questions.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// https://localhost:44320/Question/Details/
        /// </summary>
        /// <param name="id">Question`s id.</param>
        /// <returns></returns>
        public async Task<ActionResult> Details(int id)
        {
            var question = await QuestionService.GetQuestionDTOByIdAsync(id);

            if (question == null)
                return RedirectToAction("NotFound", "Error");
            ViewBag.Id = id;
            ViewBag.Viewed = question.Viewed;

            return View(mapper.Map<Feedback>(question));
        }

        /// <summary>
        /// https://localhost:44320/Question/Details/
        /// </summary>
        /// <param name="id">Question`s id.</param>
        /// <param name="form">Form collection.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Details(int id, FormCollection form)
        {
            var question = await QuestionService.GetQuestionDTOByIdAsync(id);

            if (question == null)
                return RedirectToAction("NotFound", "Error");

            ViewBag.Viewed = question.Viewed;

            question.Viewed = true;

            await QuestionService.UpdateAsync(question);

            return RedirectToAction("Questions");
        }

        /// <summary>
        /// https://localhost:44320/Question/Delete/
        /// </summary>
        /// <param name="id">Question`s id.</param>
        /// <param name="collection">Form collection.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            var question = await QuestionService.GetQuestionDTOByIdAsync(id);

            if (question == null)
                return RedirectToAction("NotFound", "Error");

            await QuestionService.DeleteAsync(id);

            return RedirectToAction("Questions");
        }
    }
}
