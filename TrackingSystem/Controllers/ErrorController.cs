﻿using System.Web.Mvc;

namespace TrackingSystem.Controllers
{
    /// <summary>
    /// Error controller.
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Diplay not found page.
        /// </summary>
        /// <returns>Not found page.</returns>
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        /// <summary>
        /// Diplay internal page.
        /// </summary>
        /// <returns>Internal page</returns>
        public ActionResult Internal()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}