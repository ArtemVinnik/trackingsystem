﻿using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TrackingSystem.Enums;
using TrackingSystem.ViewModels;
using TrackingSystemBLL.DTO;
using TrackingSystemBLL.Interfaces;

namespace TrackingSystem.Controllers
{
    /// <summary>
    /// Task controller
    /// </summary>
    [Authorize]
    public class TaskController : Controller
    {
        private readonly IMapper mapper;

        /// <summary>
        /// Construcntor.
        /// </summary>
        /// <param name="mapper">Mapper</param>
        public TaskController(IMapper mapper)
        {
            this.mapper = mapper;
        }

        private ITaskService TaskService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ITaskService>();
            }
        }

        private IEmployeeService EmployeeService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmployeeService>();
            }
        }

        private IEmailService EmailService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IEmailService>();
            }
        }

        /// <summary>
        /// https://localhost:44320/Task/Tasks
        /// </summary>
        /// <param name="page">Namber of page</param>
        /// <returns></returns>
        public async Task<ActionResult> Tasks(int? page)
        {
            var employeeId = (await EmployeeService.GetEmployeeDTOByUserNameAsync(User.Identity.Name)).Id;

            IEnumerable<DisplayTask> tasks;

            if (User.IsInRole("admin"))
            {
                tasks = mapper.Map<IEnumerable<DisplayTask>>((await TaskService.GetAllTasksDTOAsync()).Where(t => t.IsDeleted == false).OrderBy(t => t.Status));
            }
            else
            {
                tasks = mapper.Map<IEnumerable<DisplayTask>>((await TaskService.GetAllTasksDTOAsync()).Where(t => t.EmployeeId == employeeId && t.IsDeleted == false).OrderBy(t => t.Status));
            }

            int pageSize = 8;
            int pageNumber = (page ?? 1);

            return View(tasks.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// https://localhost:44320/Task/Details/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <returns></returns>
        public async Task<ActionResult> Details(int id)
        {
            var task = await TaskService.GetTaskDTOByIdAsync(id);

            if (task == null)
                return RedirectToAction("NotFound", "Error");

            var model = mapper.Map<TaskDetails>(task);

            model.EmployeeEmail = (await EmployeeService.GetEmployeeDTOByIdAsync(task.EmployeeId)).Email;

            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Task/Details/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <param name="form">Form collection.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Details(int id, FormCollection form)
        {
            var task = mapper.Map<TaskDTO>(mapper.Map<CreateTask>(await TaskService.GetTaskDTOByIdAsync(id)));

            task.Expired = DateTime.Now;
            task.Creating = mapper.Map<TaskDTO>(mapper.Map<TaskDetails>(await TaskService.GetTaskDTOByIdAsync(id))).Creating;

            if (task.Deadline >= DateTime.Now)
            {
                task.Status = (int)Status.Submitted;
            }
            else
            {
                task.Status = (int)Status.SubmittedLate;
            }

            task.Id = id;

            await TaskService.UpdateAsync(task);

            return RedirectToAction("Tasks");
        }

        /// <summary>
        /// https://localhost:44320/Task/Create
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create()
        {
            SelectList employees = new SelectList(await EmployeeService.GetAllEmployeesDTOAsync(), "Id", "Email");

            ViewBag.Employees = employees;

            return View();
        }

        /// <summary>
        /// https://localhost:44320/Task/Create
        /// </summary>
        /// <param name="model">Create task model.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateTask model)
        {
            if (model.Deadline <= DateTime.Now || model.Deadline >= DateTime.Now.AddYears(1))
                ModelState.AddModelError("Deadline", "Date out of range");

            if (ModelState.IsValid)
            {
                var task = mapper.Map<TaskDTO>(model);
                task.Creating = DateTime.Now;

                await TaskService.CreateAsync(task);

                var taskId = await TaskService.GetIdOfTaskByEmployeeIdAsync(model.EmployeeId);

                var callbackUrl = Url.Action("Details", "Task", new { id = taskId }, protocol: Request.Url.Scheme);

                await EmailService.SendAsync(task, "Task created.<br>Go to the task tab <a href=\"" + callbackUrl + "\">here</a>");

                return RedirectToAction("Tasks");
            }

            SelectList employees = new SelectList(await EmployeeService.GetAllEmployeesDTOAsync(), "Id", "Email");

            ViewBag.Employees = employees;

            return View(model);
        }

        /// <summary>
        /// https://localhost:44320/Task/Edit/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(int id)
        {
            var task = await TaskService.GetTaskDTOByIdAsync(id);

            if (task == null)
                return RedirectToAction("NotFound", "Error");

            SelectList employees = new SelectList(await EmployeeService.GetAllEmployeesDTOAsync(), "Id", "Email");

            ViewBag.Employees = employees;

            return View(mapper.Map<CreateTask>(await TaskService.GetTaskDTOByIdAsync(id)));
        }

        /// <summary>
        /// https://localhost:44320/Task/Edit/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <param name="task">Create task model.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(int id, CreateTask task)
        {
            if (task.Deadline <= DateTime.Now || task.Deadline >= DateTime.Now.AddYears(1))
                ModelState.AddModelError("Deadline", "Date out of range");

            if (ModelState.IsValid)
            {
                var lastVersionOfTask = await TaskService.GetTaskDTOByIdAsync(id);

                var model = mapper.Map<TaskDTO>(task);
                model.Id = id;
                model.Status = (int)Status.Updated;
                model.Creating = lastVersionOfTask.Creating;
                model.Expired = lastVersionOfTask.Expired;


                var operationDetails = await TaskService.UpdateAsync(model);

                await EmailService.SendAsync(model, "Task updated");

                if (operationDetails.Succeeded)
                {
                    return RedirectToAction("Tasks");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }

            SelectList employees = new SelectList(await EmployeeService.GetAllEmployeesDTOAsync(), "Id", "Email");

            ViewBag.Employees = employees;

            return View(task);
        }

        /// <summary>
        /// https://localhost:44320/Task/Delete/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(int id)
        {
            var task = await TaskService.GetTaskDTOByIdAsync(id);

            if (task == null)
                return RedirectToAction("NotFound", "Error");

            return View(mapper.Map<TaskDetails>(await TaskService.GetTaskDTOByIdAsync(id)));
        }

        /// <summary>
        /// https://localhost:44320/Task/Delete/
        /// </summary>
        /// <param name="id">Task`s id.</param>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            var task = await TaskService.GetTaskDTOByIdAsync(id);

            if (task == null)
                return RedirectToAction("NotFound", "Error");

            await TaskService.DeleteAsync(id);

            return RedirectToAction("Tasks");
        }
    }
}
