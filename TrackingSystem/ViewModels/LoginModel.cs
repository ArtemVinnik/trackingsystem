﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Login view model.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Email of employee.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Pssword of employee.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Remember to the system.
        /// </summary>
        [DisplayName("Remember me")]
        public bool Remember { get; set; }
    }
}