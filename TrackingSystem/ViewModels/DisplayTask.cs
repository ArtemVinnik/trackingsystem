﻿using System;
using TrackingSystem.Enums;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Model for display task.
    /// </summary>
    public class DisplayTask
    {
        /// <summary>
        /// Id of task.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Topic.
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Status.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Date of creating.
        /// </summary
        public DateTime Creating { get; set; }

        /// <summary>
        /// Deadline.
        /// </summary>
        public DateTime Deadline { get; set; }
    }
}