﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Feedback model.
    /// </summary>
    public class Feedback
    {
        /// <summary>
        /// Customer`s name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Customer`s email.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Subject of email.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Text of message.
        /// </summary>
        [Required]
        public string Message { get; set; }
    }
}