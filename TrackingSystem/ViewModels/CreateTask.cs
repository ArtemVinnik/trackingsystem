﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TrackingSystem.Enums;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// View model for creating task.
    /// </summary>
    public class CreateTask
    {
        /// <summary>
        /// Topic
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Instruction to task.
        /// </summary>
        public string Instruction { get; set; }

        /// <summary>
        /// Deadline.
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Employee id.
        /// </summary>
        [DisplayName("Email")]
        public string EmployeeId { get; set; }
    }
}