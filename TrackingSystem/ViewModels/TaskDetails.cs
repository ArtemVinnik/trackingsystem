﻿using System;
using System.ComponentModel;
using TrackingSystem.Enums;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Task details.
    /// </summary>
    public class TaskDetails
    {
        /// <summary>
        /// Topic
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Instruction to task.
        /// </summary>
        public string Instruction { get; set; }

        /// <summary>
        /// Date of creating.
        /// </summary
        public DateTime Creating { get; set; }

        /// <summary>
        /// Deadline.
        /// </summary>
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Time when it was handed over.
        /// </summary>
        [DisplayName("Turn")]
        public DateTime Expired { get; set; }

        /// <summary>
        /// Employee email.
        /// </summary>
        [DisplayName("Employee email")]
        public string EmployeeEmail { get; set; }
    }
}