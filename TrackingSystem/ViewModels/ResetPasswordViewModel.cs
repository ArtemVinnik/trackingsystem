﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Reset password view model
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// Email of employee.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Confirm password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Reset password token.
        /// </summary>
        public string Code { get; set; }
    }
}