﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Desplay feedback model.
    /// </summary>
    public class FeedbackDesplay
    {
        /// <summary>
        /// Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer`s name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Customer`s email.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Subject of email.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Viewed by admin.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public bool Viewed { get; set; }
    }
}