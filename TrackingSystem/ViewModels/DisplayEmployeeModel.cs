﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Display employee model
    /// </summary>
    public class DisplayEmployeeModel
    {
        /// <summary>
        /// Id.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        [DisplayName("Full name")]
        public string FullName { get; set; }

        /// <summary>
        /// Date of Birth
        /// </summary>
        [DisplayName("Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
    }
}