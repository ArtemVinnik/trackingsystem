﻿using System.ComponentModel.DataAnnotations;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Forgot password model
    /// </summary>
    public class ForgotPasswordModel
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}