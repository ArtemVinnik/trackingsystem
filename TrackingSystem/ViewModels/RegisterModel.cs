﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Register view model.
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// Email of employee.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Employee name.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your name should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Employee surname.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your surname should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Surname { get; set; }

        /// <summary>
        /// Employee date of Birth.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Role id.
        /// </summary>
        [DisplayName("Role")]
        public int RoleId { get; set; }
    }
}