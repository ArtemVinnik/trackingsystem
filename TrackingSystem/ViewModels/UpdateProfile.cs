﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TrackingSystem.ViewModels
{
    /// <summary>
    /// Update profile view model.
    /// </summary>
    public class UpdateProfile
    {
        /// <summary>
        /// Email of employee.
        /// </summary>
        [Required]
        [HiddenInput(DisplayValue = false)]
        public string Email { get; set; }

        /// <summary>
        /// Employee name.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your name should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Employee surname.
        /// </summary>
        [Required]
        [RegularExpression("^[A-Za-zА-Яа-я][A-Za-zА-Яа-я]+$", ErrorMessage = "Your surname should only contain letters.")]
        [StringLength(15, MinimumLength = 3)]
        public string Surname { get; set; }

        /// <summary>
        /// Employee date of Birth.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Date of Birth")]
        public DateTime DateOfBirth { get; set; }
    }
}