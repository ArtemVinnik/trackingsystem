﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using TrackingSystemDAL.Entities;

namespace TrackingSystemDAL.EF
{
    /// <summary>
    /// Data context class.
    /// </summary>
    public class ApplicationContext : IdentityDbContext<ApplicationEmployee>
    {
        /// <summary>
        /// Constructor of context.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        public ApplicationContext(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// The property that allows to interact with the table EmployeeProfiles.
        /// </summary>
        public DbSet<EmployeeProfile> EmployeeProfiles { get; set; }

        /// <summary>
        /// The property that allows to interact with the table WorkTask.
        /// </summary>
        public DbSet<WorkTask> WorkTasks { get; set; }

        /// <summary>
        /// The property that allows to interact with the table CustomerQuestion.
        /// </summary>
        public DbSet<CustomerQuestion> CustomerQuestions { get; set; }

        /// <summary>
        /// Constructor for migrations.
        /// </summary>
        public ApplicationContext() : base("TrackingSystemConnection")
        {
        }
    }
}