﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;
using TrackingSystemDAL.EF;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Identity;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemDAL.Repositories
{ 
    /// <summary>
    /// Identity unit of work.
    /// </summary>
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext applicationContext;

        private readonly ApplicationEmployeeManager userManager;
        private readonly ApplicationRoleManager roleManager;
        private readonly IEmployeeProfileRepository profileRepository;
        private readonly IRepository<WorkTask> taskRepository;
        private readonly IRepository<CustomerQuestion> questionRepository;

        /// <summary>
        /// Employee repository.
        /// </summary>
        public ApplicationEmployeeManager EmployeeManager
        {
            get
            {
                return userManager;
            }
        }

        /// <summary>
        /// Employee profile repository.
        /// </summary>
        public IEmployeeProfileRepository EmployeeProfileManager
        {
            get
            {
                return profileRepository;
            }
        }

        /// <summary>
        /// Role repository.
        /// </summary>
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return roleManager;
            }
        }

        /// <summary>
        /// Task repository.
        /// </summary>
        public IRepository<WorkTask> TaskRepository
        {
            get 
            {
                return taskRepository;
            }
        }

        /// <summary>
        /// Question repository.
        /// </summary>
        public IRepository<CustomerQuestion> QuestionRepository
        {
            get
            {
                return questionRepository;
            }
        }


        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        public IdentityUnitOfWork(string connectionString)
        {
            applicationContext = new ApplicationContext(connectionString);

            userManager = new ApplicationEmployeeManager(new UserStore<ApplicationEmployee>(applicationContext));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(applicationContext));
            profileRepository = new EmployeeProfilesRepository(applicationContext);
            taskRepository = new WorkTasksRepositry(applicationContext);
            questionRepository = new CustomerQuestionsRepository(applicationContext);
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Save changes in database.
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            await applicationContext.SaveChangesAsync();
        }

        /// <summary>
        /// Save changes in database.
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            applicationContext.SaveChanges();
        }

        private bool disposed = false;

        /// <summary>
        /// Mechanism for freeing unmanaged resources.
        /// </summary>
        /// <param name="disposing">Disposing.</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    profileRepository.Dispose();
                    taskRepository.Dispose();
                    questionRepository.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
