﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using TrackingSystemDAL.EF;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemDAL.Repositories
{
    /// <summary>
    /// Employees repository provide base methods (CRUD)
    /// </summary>
    public class EmployeeProfilesRepository : IEmployeeProfileRepository
    {
        private readonly ApplicationContext _applicationContext;

        /// <summary>
        /// Constructor of class.
        /// </summary>
        /// <param name="applicationContext">Application context</param>
        public EmployeeProfilesRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        /// <summary>
        /// Created a new object.
        /// </summary>
        /// <param name="employee">Object instance.</param>
        /// <returns>Object instance.</returns>
        public EmployeeProfile Create(EmployeeProfile employee)
        {
            //_applicationContext.EmployeeProfiles.Add(employee);
            //_applicationContext.SaveChanges();

            return employee;
        }

        /// <summary>
        /// Remove object from database.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>true if object exist; false if not.</returns>
        public async Task<bool> DeleteAsync(string id)
        {
            var item = await _applicationContext.EmployeeProfiles.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _applicationContext.EmployeeProfiles.Remove(item);
            }

            return result;
        }

        /// <summary>
        /// Releas context.
        /// </summary>
        public void Dispose()
        {
            _applicationContext.Dispose();
        }

        /// <summary>
        /// Get all objects.
        /// </summary>
        /// <returns>Object collections.</returns>
        public async Task<IEnumerable<EmployeeProfile>> GetAllAsync()
        {
            return await _applicationContext.EmployeeProfiles.ToListAsync();
        }

        /// <summary>
        /// Get object by id.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>Object instance.</returns>
        public async Task<EmployeeProfile> GetByIdAsync(string id)
        {
            return await _applicationContext.EmployeeProfiles.FindAsync(id);
        }

        /// <summary>
        /// Update object.
        /// </summary>
        /// <param name="employee">Object instance.</param>
        /// <returns>true if object exist; false if not.</returns>
        public bool Update(EmployeeProfile employee)
        {
            var item = _applicationContext.EmployeeProfiles.Attach(employee);
            _applicationContext.Entry(employee).State = EntityState.Modified;

            return item != null;
        }
    }
}
