﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemDAL.EF;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemDAL.Repositories
{
    /// <summary>
    /// Class of work with database in table CustomerQuestion.
    /// </summary>
    public class CustomerQuestionsRepository : IRepository<CustomerQuestion>
    {
        private readonly ApplicationContext _applicationContext;

        /// <summary>
        /// Constructor of class.
        /// </summary>
        /// <param name="applicationContext">Application context</param>
        public CustomerQuestionsRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        /// <summary>
        /// Create an question.
        /// </summary>
        /// <param name="question">Object instance.</param>
        /// <returns>Object instance.</returns>
        public CustomerQuestion Create(CustomerQuestion question)
        {
            _applicationContext.CustomerQuestions.Add(question);

            return question;
        }

        /// <summary>
        /// Delete question.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>true if question exist.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _applicationContext.CustomerQuestions.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _applicationContext.CustomerQuestions.Remove(item);
            }

            return result;
        }

        /// <summary>
        /// Releas context.
        /// </summary>
        public void Dispose()
        {
            _applicationContext.Dispose();
        }

        /// <summary>
        /// Get all questions.
        /// </summary>
        /// <returns>Collection of questions.</returns>
        public IEnumerable<CustomerQuestion> GetAll()
        {
            return _applicationContext.CustomerQuestions.ToList();
        }

        /// <summary>
        /// Get all questions.
        /// </summary>
        /// <returns>Collection of questions.</returns>
        public async Task<IEnumerable<CustomerQuestion>> GetAllAsync()
        {
            return await _applicationContext.CustomerQuestions.ToListAsync();
        }

        /// <summary>
        /// Get question by id.
        /// </summary>
        /// <param name="id">Id of question.</param>
        /// <returns>Object instance.</returns>
        public async Task<CustomerQuestion> GetByIdAsync(int id)
        {
            return await _applicationContext.CustomerQuestions.FindAsync(id);
        }

        /// <summary>
        /// Update question.
        /// </summary>
        /// <param name="question">Object instance.</param>
        /// <returns>true if question exist.</returns>
        public bool Update(CustomerQuestion question)
        {
            var existingQuestion = _applicationContext.CustomerQuestions.Find(question.Id);
            _applicationContext.Entry(existingQuestion).CurrentValues.SetValues(question);

            return existingQuestion != null;
        }
    }
}
