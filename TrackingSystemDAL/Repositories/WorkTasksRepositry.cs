﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TrackingSystemDAL.EF;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Interfaces;

namespace TrackingSystemDAL.Repositories
{
    /// <summary>
    /// Class of work with database in table WorkTask.
    /// </summary>
    public class WorkTasksRepositry : IRepository<WorkTask>
    {
        private readonly ApplicationContext _applicationContext;

        /// <summary>
        /// Constructor of class.
        /// </summary>
        /// <param name="applicationContext">Application context</param>
        public WorkTasksRepositry(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        /// <summary>
        /// Create a task.
        /// </summary>
        /// <param name="task">Object instance.</param>
        /// <returns>Object instance.</returns>
        public WorkTask Create(WorkTask task)
        {
            task.Expired = DateTime.Parse("10.10.2021");
            _applicationContext.WorkTasks.Add(task);

            return task;
        }

        /// <summary>
        /// Delete task.
        /// </summary>
        /// <param name="id">Id of task.</param>
        /// <returns>true if task exist.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _applicationContext.WorkTasks.FirstOrDefaultAsync(t => t.Id == id);
            var result = item != null;

            if (result)
            {
                item.IsDeleted = true;
                Update(item);
            }

            return result;
        }

        /// <summary>
        /// Releas context.
        /// </summary>
        public void Dispose()
        {
            _applicationContext.Dispose();
        }

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of tasks.</returns>
        public async Task<IEnumerable<WorkTask>> GetAllAsync()
        {
            return await _applicationContext.WorkTasks.ToListAsync();
        }

        /// <summary>
        /// Get all tasks.
        /// </summary>
        /// <returns>Collection of tasks.</returns>
        public IEnumerable<WorkTask> GetAll()
        {
            return _applicationContext.WorkTasks.ToList();
        }

        /// <summary>
        /// Get task by id.
        /// </summary>
        /// <param name="id">Id of task.</param>
        /// <returns>Object instance.</returns>
        public async Task<WorkTask> GetByIdAsync(int id)
        {
            return await _applicationContext.WorkTasks.FindAsync(id);
        }

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="task">Object instance.</param>
        /// <returns>true if task exist.</returns>
        public bool Update(WorkTask task)
        {
            var existingTask = _applicationContext.WorkTasks.Find(task.Id);
            _applicationContext.Entry(existingTask).CurrentValues.SetValues(task);

            return existingTask != null;
        }
    }
}
