﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackingSystemDAL.Entities;

namespace TrackingSystemDAL.Interfaces
{
    /// <summary>
    /// Employees repository provide base methods (CRUD)
    /// </summary>
    public interface IEmployeeProfileRepository : IDisposable
    {
        /// <summary>
        /// Created a new object.
        /// </summary>
        /// <param name="employee">Object instance.</param>
        /// <returns>Object instance.</returns>
        EmployeeProfile Create(EmployeeProfile employee);

        /// <summary>
        /// Get all objects.
        /// </summary>
        /// <returns>Object collections.</returns>
        Task<IEnumerable<EmployeeProfile>> GetAllAsync();

        /// <summary>
        /// Get object by id.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>Object instance.</returns>
        Task<EmployeeProfile> GetByIdAsync(string id);

        /// <summary>
        /// Update object.
        /// </summary>
        /// <param name="employee">Object instance.</param>
        /// <returns>true if object exist; false if not.</returns>
        bool Update(EmployeeProfile employee);

        /// <summary>
        /// Remove object from database.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>true if object exist; false if not.</returns>
        Task<bool> DeleteAsync(string id);
    }
}
