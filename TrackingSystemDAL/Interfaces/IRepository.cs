﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TrackingSystemDAL.Interfaces
{
    /// <summary>
    /// Interface of repository
    /// </summary>
    /// <typeparam name="T">Object instance</typeparam>
    public interface IRepository<T> : IDisposable
    {
        /// <summary>
        /// Created an object.
        /// </summary>
        /// <param name="t">Object instance.</param>
        /// <returns>Object instance</returns>
        T Create(T t);

        /// <summary>
        /// Get all of objects.
        /// </summary>
        /// <returns>Collection of object instance.</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Get all of objects.
        /// </summary>
        /// <returns>Collection of object instance.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Get by id of object.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>Object instance.</returns>
        Task<T> GetByIdAsync(int id);

        /// <summary>
        /// Updated an object.
        /// </summary>
        /// <param name="t">Object instance.</param>
        /// <returns>true if object exist</returns>
        bool Update(T t);

        /// <summary>
        /// Deleted an object.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <returns>true if object exist</returns>
        Task<bool> DeleteAsync(int id);
    }
}
