﻿using System;
using System.Threading.Tasks;
using TrackingSystemDAL.Entities;
using TrackingSystemDAL.Identity;

namespace TrackingSystemDAL.Interfaces
{
    /// <summary>
    /// Interface of pattern unit of work.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Employee manager property.
        /// </summary>
        ApplicationEmployeeManager EmployeeManager { get; }

        /// <summary>
        /// Employee profile manager property.
        /// </summary>
        IEmployeeProfileRepository EmployeeProfileManager { get; }

        /// <summary>
        /// Task repository property.
        /// </summary>
        IRepository<WorkTask> TaskRepository { get; }

        /// <summary>
        /// Role manager property.
        /// </summary>
        ApplicationRoleManager RoleManager { get; }

        /// <summary>
        /// Question repository.
        /// </summary>
        IRepository<CustomerQuestion> QuestionRepository { get; }

        /// <summary>
        /// Save changes in database.
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();

        /// <summary>
        /// Save changes in database.
        /// </summary>
        /// <returns></returns>
        void Save();
    }
}
