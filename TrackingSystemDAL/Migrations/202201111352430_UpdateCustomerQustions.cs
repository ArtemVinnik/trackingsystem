﻿namespace TrackingSystemDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCustomerQustions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerQuestions", "Viewed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomerQuestions", "Viewed");
        }
    }
}
