﻿namespace TrackingSystemDAL.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using TrackingSystemDAL.EF;
    using TrackingSystemDAL.Entities;
    using TrackingSystemDAL.Identity;
    using TrackingSystemDAL.Interfaces;
    using TrackingSystemDAL.Repositories;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationContext context)
        {
            ApplicationRole role1 = new ApplicationRole { Name = "admin" };
            ApplicationRole role2 = new ApplicationRole { Name = "employee" };

            ApplicationEmployee user1 = new ApplicationEmployee { UserName = "ivan@trackingsystem.admin", Email = "ivan@trackingsystem.admin" };
            ApplicationEmployee user2 = new ApplicationEmployee { UserName = "vasya@user.com", Email = "vasya@user.com" };
            string password = "1234_Abcd";

            ApplicationRoleManager roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));
            ApplicationEmployeeManager userManager = new ApplicationEmployeeManager(new UserStore<ApplicationEmployee>(context));
            IEmployeeProfileRepository profileRepository = new EmployeeProfilesRepository(context);

            if (roleManager.FindByName(role1.Name) == null)
            {

                roleManager.Create(role1);
                context.SaveChanges();

                roleManager.Create(role2);
                context.SaveChanges();

                EmployeeProfile userProfile1 = new EmployeeProfile
                {
                    Id = user1.Id,
                    Name = "Ivan",
                    Surname = "Ivanov",
                    DateOfBirth = DateTime.Parse("12.12.1990")
                };

                EmployeeProfile userProfile2 = new EmployeeProfile
                {
                    Id = user2.Id,
                    Name = "Vasya",
                    Surname = "Ivanov",
                    DateOfBirth = DateTime.Parse("12.12.1996")
                };

                user1.EmployeeProfile = userProfile1;
                user2.EmployeeProfile = userProfile2;

                userManager.Create(user1, password);
                context.SaveChanges();

                userManager.Create(user2, password);
                context.SaveChanges();

                //profileRepository.Create(userProfile1);
                //profileRepository.Create(userProfile2);

                userManager.AddToRole(userManager.FindByName(user1.UserName).Id, "admin");
                context.SaveChanges();

                userManager.AddToRole(userManager.FindByName(user2.UserName).Id, "employee");
                context.SaveChanges();
            }
            base.Seed(context);
        }
    }
}
