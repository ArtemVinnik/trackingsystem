﻿namespace TrackingSystemDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCustomerQustions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerQuestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(nullable: false),
                        Subject = c.String(),
                        Message = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CustomerQuestions");
        }
    }
}
