﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TrackingSystemDAL.Entities;

namespace TrackingSystemDAL.Identity
{
    /// <summary>
    /// Employee management class.
    /// </summary>
    public class ApplicationEmployeeManager : UserManager<ApplicationEmployee>
    {
        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="store">An abstraction for a store which manages employee accounts.</param>
        public ApplicationEmployeeManager(IUserStore<ApplicationEmployee> store) : base(store)
        {
            var dataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Tracking system");
            if (dataProtectionProvider != null)
            {
                this.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationEmployee>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }
    }

}
