﻿using Microsoft.AspNet.Identity;
using TrackingSystemDAL.Entities;

namespace TrackingSystemDAL.Identity
{
    /// <summary>
    /// Role management class.
    /// </summary>
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="store">An abstraction for a storage and management of roles.</param>
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> store) : base(store)
        {
        }
    }
}
