﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrackingSystemDAL.Entities
{
    /// <summary>
    /// Class of information about employee.
    /// </summary>
    public class EmployeeProfile
    {
        /// <summary>
        /// Id of employee.
        /// </summary>
        [Key]
        [ForeignKey("ApplicationEmployee")]
        public string Id { get; set; }

        /// <summary>
        /// Employee name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Employee surname.
        /// </summary>
        [Required]
        public string Surname { get; set; }

        /// <summary>
        /// Employee date of Birth.
        /// </summary>
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// One-to-one relationship.
        /// </summary>
        public virtual ApplicationEmployee ApplicationEmployee { get; set; }

        /// <summary>
        /// One-to-many relationship.
        /// </summary>
        public virtual ICollection<WorkTask> WorkTasks { get; set; }
    }
}
