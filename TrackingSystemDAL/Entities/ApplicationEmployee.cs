﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace TrackingSystemDAL.Entities
{
    /// <summary>
    /// Class of employee.
    /// </summary>
    public class ApplicationEmployee : IdentityUser
    {
        /// <summary>
        /// One-to-one relationship.
        /// </summary>
        [Required]
        public virtual EmployeeProfile EmployeeProfile { get; set; }
    }
}
