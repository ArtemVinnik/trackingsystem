﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TrackingSystemDAL.Entities
{
    /// <summary>
    /// Class of employee`s role.
    /// </summary>
    public class ApplicationRole : IdentityRole
    {
    }
}
