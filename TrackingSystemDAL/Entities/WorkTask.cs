﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrackingSystemDAL.Entities
{
    /// <summary>
    /// Task 
    /// </summary>
    public class WorkTask
    {
        /// <summary>
        /// Id of task.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Topic
        /// </summary>
        [Required]
        public string Topic { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [Required]
        public int Status { get; set; }

        /// <summary>
        /// Instruction to task.
        /// </summary>
        [Required]
        public string Instruction { get; set; }

        /// <summary>
        /// Date of creating.
        /// </summary>
        [Required]
        public DateTime Creating { get; set; }

        /// <summary>
        /// Deadline.
        /// </summary>
        [Required]
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Time when it was handed over.
        /// </summary>
        public DateTime? Expired { get; set; }

        /// <summary>
        /// Property for soft deleting.
        /// </summary>
        public bool IsDeleted { get; set; }

        [ForeignKey("EmployeeProfile")]
        public string EmployeeProfileId { get; set; }

        /// <summary>
        /// One-to-many relationship.
        /// </summary>
        public virtual EmployeeProfile EmployeeProfile { get; set; }
    }
}
